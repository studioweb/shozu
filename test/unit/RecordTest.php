<?php
require_once __DIR__ . '/test_helper.php';

# FIXME: Neither PHP nor PHPUnit allows to stub instance methods at runtime, so
# \shozu\Record can not be tested conveniently.
# So we create the following Record class (extending \shozu\Record) and use it
# as the SUT.
# A better solution would be to use PHP runkit (or similar) and define the
# method at runtime on \shozu\Record, in each test that needs it.
class Record extends \shozu\Record
{
    # Records needs setTableDefinition() method to be defined.
    protected function setTableDefinition()
    {
        $this->addColumn(array(
            'name'          => 'name',
            'type'          => 'string',
            'length'        => 64,
        ));
        $this->addColumn(array(
            'name'          => 'snake_prop',
            'type'          => 'string',
            'length'        => 64
        ));
        $this->addColumn(array(
            'name'          => 'notblank_prop',
            'type'          => 'string',
            'length'        => 64,
            'validators'    => array('notblank'),
            'notnull'       => true
        ));
        $this->addColumn(array(
            'name'          => 'int_nullable_prop',
            'type'          => 'integer',
            'nullable'      => true
        ));
        $this->addColumn(array(
            'name'          => 'int_prop',
            'type'          => 'integer'
        ));
    }
}

class RecordTest extends PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        $record = new Record(array(
            'notblank_prop'  => 'Some value'
        ));

        $this->record = $record;
    }

    public function testRecordWithValidAttributesIsValid()
    {
        $this->assertTrue($this->record->isValid());
    }

    public function testRecordWithAnInvalidAttributeIsNotValid()
    {
        $this->record->notblank_prop = '';

        $this->assertFalse($this->record->isValid());
    }

    public function testRecordHasColumnWhenColumnExists()
    {
        $this->assertTrue($this->record->hasColumn('name'));
    }

    public function testRecordHasColumnWhenColumnDontExists()
    {
        $this->assertFalse($this->record->hasColumn('non_existent_column'));
    }

    public function testRecordMagicSetterSetAttribute()
    {
        $value = 'Some value';
        $this->record->setName($value);

        $this->assertEquals($value, $this->record->name);
    }

    public function testRecordMagicSetterInflectedAttribute()
    {
        $value = 'Some value';
        $this->record->setSnakeProp($value);

        $this->assertEquals($value, $this->record->snake_prop);
    }

    public function testRecordMagicGetterGetAttribute()
    {
        $value = 'Some value';
        $this->record->name = $value;

        $this->assertEquals($value, $this->record->getName());
    }

    public function testRecordMagicGetterInflectAttribute()
    {
        $value = 'Some value';
        $this->record->snake_prop = $value;

        $this->assertEquals($value, $this->record->getSnakeProp());
    }

    public function testRecordMagicGetterGetNullAttribute()
    {
        $this->assertEquals(null, $this->record->getName());
    }

    public function testRecordCallNonExistentGetterRaiseBadMethodCallException()
    {
        $this->setExpectedException('BadMethodCallException');

        $this->record->getNonExistentGetter();
    }

    public function testRecordCallNonExistentSetterRaiseBadMethodCallException()
    {
        $this->setExpectedException('BadMethodCallException');

        $this->record->setNonExistentSetter(null);
    }

    public function testValuesDiffReturnExpectedValuesOnPropertiesChanges() {
        $record = new Record(array(
            'notblank_prop'  => 'Some value',
            'int_prop' => 5,
            'snake_prop' => 'Some other value'

        ));

        // From null to 0 (as null == 0 == false)
        $record->int_nullable_prop = 0;
        // Same value but with an other type
        $record->int_prop = '5';
        // changing string value
        $record->snake_prop = 'An other value';
        // same value
        $record->notblank_prop = 'Some value';

        $expected_diff_values = array(
            'snake_prop' => array(
                'Some other value',
                'An other value'
            ),
            'int_nullable_prop' => array(
                null,
                0
            )
        );

        $values_diff = $this->invokeMethod($record, 'valuesDiff');
        $this->assertJsonStringEqualsJsonString(json_encode($expected_diff_values), json_encode($values_diff));
    }

    /**
     * Call protected/private method of a class.
     *
     * @param object &$object    Instantiated object that we will run method on.
     * @param string $methodName Method name to call
     * @param array  $parameters Array of parameters to pass into method.
     *
     * @return mixed Method return.
     */
    public function invokeMethod(&$object, $methodName, array $parameters = array())
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }
}
